import time
import math
import numpy as np
from pylab import *

def write_arrays(f_name, open_type, arr1, arr2):
        f = open(f_name, open_type)
        s = ''

        for j in range(0, len(arr1)):
                s += str(arr1[j]) + ','
                
        for j in range(1, len(arr2)):
                s += str(arr2[j]) + ','
                
        s += '\n'
        f.write(s)
        f.close()

def TDMA(aw,ae,ap,q,res,nx,type):
	P = [0]*nx
	Q = [0]*nx

	if type == "val":
		P[0] = 0.
		Q[0] = res[0]
		
	for i in range(1, nx-1):
		denom = 1./(aw[i]*P[i-1] + ap[i])
	
		P[i] = -ae[i] * denom
		Q[i] = (q[i] - aw[i]*Q[i-1]) * denom
	
	for i in range(nx-2, 0, -1):
		res[i] = P[i]*res[i+1] + Q[i]
		

def integrate(t_0, t_end):
	t = t_0

	# Time stepping
	while t < t_end:
		t += dt
		print t
		# CHT fluid-solid iterations
		tol_conj = 1.0
		iteration_cht = 0
		while tol_conj > rel_tol:

			iteration_cht += 1
			if(iteration_cht > 4):
				break
			
			T_star = [0.0]*(nx+2)
			for i in range(1, nx+1):
				T_star[i] = T_s[i][0]
			
			tol = 1.0
			iteration = 0
		
			# Iterative inner-time solver
			# Solve for fluid, set fixed value BC at interface
			for i in range (1, nx+1):
				T_f[i][nr_f+1] = T_s[i][0]

			while tol > rel_tol:
				iteration += 1
				tol = 0.0

				for i in range(1, nx+1):
					for j in range(nr_f, 0, -1):
						res = T_f[i][j] - (a_f_n[i][j]*T_f[i][j+1] + a_f_s[i][j]*T_f[i][j-1] +
										   a_f_e[i][j]*T_f[i+1][j] + a_f_w[i][j]*T_f[i-1][j] +
										   a_f_p0[i][j]*T0_f[i][j])/a_f_p[i][j]
						T_f[i][j] -= res

				for j in range(nr_f, 0, -1):
					for i in range(1, nx+1):
						res = T_f[i][j] - (a_f_n[i][j]*T_f[i][j+1] + a_f_s[i][j]*T_f[i][j-1] +
										   a_f_e[i][j]*T_f[i+1][j] + a_f_w[i][j]*T_f[i-1][j] +
										   a_f_p0[i][j]*T0_f[i][j])/a_f_p[i][j]
						T_f[i][j] -= res
						tol += abs(res)
			
			# Interpolate T / axis and right BC
			for i in range(1, nx+1):
				T_f[i][0] = T_f[i][1]

			for j in range(1, nr_f+1):
				T_f[nx+1][j] = T_f[nx][j]

                        # Solve for solid, set gradient BC at interface
			# Calculate heat flux at interface
			q = [0.0]*(nx+2)
			for i in range(1, nx+1):
				q[i] = -k_f*(T_f[i][nr_f+1] - T_f[i][nr_f])/(0.5*dr_f[nr_f])
				a_s_p[i][1] -= a_s_s[i][1]
				a_s_s[i][1] = 0.0
				b_s[i][1] = dx[i]*r_s[0]*q[i]

			tol = 1.0
			while tol > rel_tol:
				iteration += 1
				tol = 0.0

				for i in range(1, nx+1):
					for j in range(nr_s, 0, -1):
						res = T_s[i][j] - (a_s_n[i][j]*T_s[i][j+1] + a_s_s[i][j]*T_s[i][j-1] +
										   a_s_e[i][j]*T_s[i+1][j] + a_s_w[i][j]*T_s[i-1][j] +
										   a_s_p0[i][j]*T0_s[i][j] + b_s[i][j])/a_s_p[i][j]
						T_s[i][j] -= res

				for j in range(nr_s, 0, -1):
					for i in range(1, nx+1):
						res = T_s[i][j] - (a_s_n[i][j]*T_s[i][j+1] + a_s_s[i][j]*T_s[i][j-1] +
										   a_s_e[i][j]*T_s[i+1][j] + a_s_w[i][j]*T_s[i-1][j] +
										   a_s_p0[i][j]*T0_s[i][j] + b_s[i][j])/a_s_p[i][j]
						T_s[i][j] -= res
						tol += abs(res)
			
                        # Interpolate T at interface BC
			for i in range(1, nx+1):
				T_f[i][nr_f+1] = T_s[i][0] = q[i]*0.5*dr_s[1]/k_s + T_s[i][1]
	
                        tol_conj = 0
			for i in range(1, nx+1):
				tol_conj += abs(T_s[i][0] - T_star[i])/T_left
                
		for i in range(1, nx+1):
			for j in range(1, nr_f+2):
				T0_f[i][j] = T_f[i][j]

		for i in range(1, nx+1):
			for j in range(0, nr_s+1):
				T0_s[i][j] = T_s[i][j]


start_time = time.time()

# Hydrodynamic and thermophysical properties
alfa_r = 0.0051
alfa_f = 1.0
alfa_s = alfa_r*alfa_f
k_r = 7.48
k_f = 1.0
k_s = k_f*k_r
rhoC_f = 1.0
rhoC_s = k_r/alfa_r*rhoC_f
conv_f = 10.0
conv_s = 0.0
T_left = 1.0
T_top = 0.0
T_init = 0.0


# Numerical settings
rel_tol = 1.0e-4

# Dimensioneless calculation
t_0 = 0.0
t_end = 0.018
dt = 0.05

nr = 40
nx = 50
eps = 1.2

r_0 = 0.0
r_int = 1.0
r_max = 1.2
x_max = 1.57

# Total amount of the points with the boundaries
dx = [0.0]*(nx + 2)
x = [0.0]*(nx + 2)
#dr = [0.0]*(nr + 2)
#r = [0.0]*(nr + 2)


# Geometrical progression of the dx distribution
eps_sum = 0.0
for i in range(0, nx):
	eps_sum += pow(eps, i)

	dx[1] = x_max/eps_sum
	x[1] = 0.5*dx[1]
	
	for i in range(2, nx+1):
		dx[i] = dx[i-1]*eps
		x[i] = x[i-1] + 0.5*dx[i-1] + 0.5*dx[i]
		
		x[nx+1] = x[nx] + dx[nx]*0.5


# Geometrical progression of the dr distribution
rel = (r_max - r_int)/r_int
nr_int = 0

eps_sum_f = 0.0
for i in range(0, nr):
        eps_sum_f += pow(eps, i)

eps_sum_s = 0.0
for i in range(0, nr):
    eps_sum_f -= pow(eps, nr-1-i)
    eps_sum_s += pow(eps, i)
    if(rel - eps_sum_s/eps_sum_f < 0):
        nr_int = i
        break

nr_int = nr-nr_int
    
eps_sum_s = 0.0
for i in range(nr_int, nr):
    eps_sum_s += pow(eps, i-nr_int)

eps_sum_f = 0.0
for i in range(0, nr_int):
    eps_sum_f += pow(eps, i)

nr_f = nr_int
nr_s = nr - nr_int

dr_f = [0.0]*(nr_f+2)
r_f = [0.0]*(nr_f+2)
dr_s = [0.0]*(nr_s+2)
r_s = [0.0]*(nr_s+2)

dr_f[nr_f] = r_int/eps_sum_f
dr_s[1] = (r_max - r_int)/eps_sum_s

r_f[nr_f+1] = r_int
r_f[nr_f] = r_int - 0.5*dr_f[nr_f]
r_s[0] = r_int
r_s[1] = r_int + 0.5*dr_s[1]

for i in range(1, nr_int):
    dr_f[nr_int-i] = dr_f[nr_int+1-i]*eps
    r_f[nr_int-i] = r_f[nr_int+1-i] - 0.5*dr_f[nr_int-i] - 0.5*dr_f[nr_int-i+1]

r_f[0] = r_f[1] - 0.5*dr_f[1]


for i in range(2, nr_s+1):
    dr_s[i] = dr_s[i-1]*eps
    r_s[i] = r_s[i-1] + 0.5*dr_s[i-1] + 0.5*dr_s[i]
    
r_s[nr_s+1] = r_s[nr_s]+0.5*dr_s[nr_s]


# Arrays of hydrodynamic and thermophysical properties

T_f = [[0.0]*(nr_f+2) for i in range(nx+2)]
T0_f = [[0.0]*(nr_f+2) for i in range(nx+2)]

T_s = [[0.0]*(nr_s+2) for i in range(nx+2)]
T0_s = [[0.0]*(nr_s+2) for i in range(nx+2)]

# Boundary conditions
for j in range(0, nr_f+2):
	T_f[0][j] = T_left
	
for i in range(0, nx+2):
	T_s[i][nr_s+1] = T_top

# Initial conditions
for i in range(1, nx+1):
	for j in range(1, nr_f+1):
		T_f[i][j] = T_init
		T0_f[i][j] = T_init

for i in range(1, nx+1):
	for j in range(1, nr_s+1):
		T_s[i][j] = T_init
		T0_s[i][j] = T_init

a_f_p = [[0.0]*(nr_f+2) for i in range(nx+2)]
a_f_e = [[0.0]*(nr_f+2) for i in range(nx+2)]
a_f_w = [[0.0]*(nr_f+2) for i in range(nx+2)]
a_f_n = [[0.0]*(nr_f+2) for i in range(nx+2)]
a_f_s = [[0.0]*(nr_f+2) for i in range(nx+2)]
a_f_p0 = [[0.0]*(nr_f+2) for i in range(nx+2)]
b_f = [[0.0]*(nr_f+2) for i in range(nx+2)]

a_s_p = [[0.0]*(nr_s+2) for i in range(nx+2)]
a_s_e = [[0.0]*(nr_s+2) for i in range(nx+2)]
a_s_w = [[0.0]*(nr_s+2) for i in range(nx+2)]
a_s_n = [[0.0]*(nr_s+2) for i in range(nx+2)]
a_s_s = [[0.0]*(nr_s+2) for i in range(nx+2)]
a_s_p0 = [[0.0]*(nr_s+2) for i in range(nx+2)]
b_s = [[0.0]*(nr_s+2) for i in range(nx+2)]

# Matrix coefficients
for i in range(1, nx+1):
	for j in range(1, nr_f+1):
		a_f_p0[i][j] = rhoC_f*r_f[j]*dr_f[j]*dx[i]/dt
		a_f_n[i][j] = k_f*(r_f[j] + 0.5*dr_f[j])*dx[i]/(0.5*dr_f[j] + 0.5*dr_f[j+1])
		a_f_s[i][j] = k_f*(r_f[j] - 0.5*dr_f[j])*dx[i]/(0.5*dr_f[j] + 0.5*dr_f[j-1])
		a_f_e[i][j] = 0.0
		a_f_w[i][j] = rhoC_f*conv_f*(1.0 - r_f[j]*r_f[j])*r_f[j]*dr_f[j]
		a_f_p[i][j] = a_f_p0[i][j] + a_f_n[i][j] + a_f_s[i][j] + rhoC_f*conv_f*(1.0 - r_f[j]*r_f[j])*r_f[j]*dr_f[j]

for i in range(1, nx+1):
	for j in range(1, nr_s+1):
		a_s_p0[i][j] = rhoC_s*r_s[j]*dr_s[j]*dx[i]/dt
		a_s_n[i][j] = k_s*(r_s[j] + 0.5*dr_s[j])*dx[i]/(0.5*dr_s[j] + 0.5*dr_s[j+1])
		a_s_s[i][j] = k_s*(r_s[j] - 0.5*dr_s[j])*dx[i]/(0.5*dr_s[j] + 0.5*dr_s[j-1])
		a_s_e[i][j] = 0.0
		a_s_w[i][j] = 0.0
		a_s_p[i][j] = a_s_p0[i][j] + a_s_n[i][j] + a_s_s[i][j]
                b_s[i][j] = 0.0

path = '2d_separate.csv'
write_arrays(path, 'w', r_f, r_s)
                
integrate(t_0=0.0, t_end=0.1)
write_arrays(path, 'a', T_f[2], T_s[2])

integrate(t_0=0.1, t_end=0.5)
write_arrays(path, 'a', T_f[2], T_s[2])

integrate(t_0=0.5, t_end=1.0)
write_arrays(path, 'a', T_f[2], T_s[2])

integrate(t_0=1.0, t_end=2.0)
write_arrays(path, 'a', T_f[2], T_s[2])

print("--- %s seconds ---" % (time.time() - start_time))
