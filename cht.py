import math
import numpy as np
from pylab import *

            
def progonka(aw,ae,ap,q,res,nx,type):
    P = [0]*nx
    Q = [0]*nx

    if type == "val":
        P[0] = 0.
        Q[0] = res[0]

    for i in range(1, nx-1):
        denom = 1./(aw[i]*P[i-1] + ap[i])
        
        P[i] = -ae[i] * denom
        Q[i] = (q[i] - aw[i]*Q[i-1]) * denom
    
    for i in range(nx-2, 0, -1):
        res[i] = P[i]*res[i+1] + Q[i]

    return res

# Thermophysical properties
alfa_r = 1.0

# Dimensioneless calculation
t_0 = 0.0
t_end = 0.01
dt = 0.001

nx = 10
nr = 10
eps = 1.2

r_0 = 0.0
r_int = 1.0
r_max = 1.2
x_max = 1.5

# Total amount of the points with the boundaries
dx = [0.0]*(nx + 2)
dr = [0.0]*(nr + 2)

x = [0.0]*(nx + 2)
r = [0.0]*(nr + 2)

# Geometrical progression of the dx distribution
eps_sum = 0.0
for i in range(0, nx):
    eps_sum += pow(eps, i)

dx[1] = x_max/eps_sum
x[1] = 0.5*dx[1]

for i in range(2, nx+1):
    dx[i] = dx[i-1]*eps
    x[i] = x[i-1] + 0.5*dx[i-1] + 0.5*dx[i]

x[nx+1] = x[nx] + dx[nx]*0.5

# Geometrical progression of the dr distribution
rel = (r_max - r_int)/r_int
nr_int = 0

eps_sum_tot = 0.0
for i in range(0, nr):
    eps_sum_tot += pow(eps, i)

eps_sum_f = 0.0
for i in range(0, nr):
    eps_sum_tot -= pow(eps, nr-1-i)
    eps_sum_f += pow(eps, i)
    
    if(rel - eps_sum_f/eps_sum_tot < 0):
        nr_int = i
        break

eps_sum_f = 0.0
for i in range(0, nr_int):
    eps_sum_f += pow(eps, i)

#nr_int += 1

dr[nr_int] = r_int/eps_sum_f
r[nr_int] = r_int - 0.5*dr[nr_int]

for i in range(1, nr_int):
    dr[nr_int-i] = dr[nr_int+1-i]*eps
    r[nr_int-i] = r[nr_int+1-i] - 0.5*dr[nr_int-i] - 0.5*dr[nr_int-i+1]

r[0] = r[1] - 0.5*dr[1]

eps_sum_s = 0.0
for i in range(nr_int, nr):
    eps_sum_s += pow(eps, i - nr_int)

dr[nr_int+1] = (r_max - r_int)/eps_sum_s
r[nr_int+1] = r[nr_int] + 0.5*dr[nr_int] + 0.5*dr[nr_int+1]
for i in range(nr_int+2, nr+1):
    dr[i] = dr[i - 1]*eps
    r[i] = r[i-1] + 0.5*dr[i-1] + 0.5*dr[i]

r[nr+1] = r[nr]+0.5*dr[nr]

T = [[0.0]*(nr+2)]*(nx+2)


# Boundary conditions
for j in range(0, nr+2):
    T[0][j] = 1.0

for i in range(0, nx+2):
    T[i][0] = 0.0

# Initial conditions
for i in range(1, nx+2):
    for j in range(1, nr+2):
        T[i][j] = 1.0

a_p = [[0.0]*(nr+2)]*(nx+2)
a_p0 = [[0.0]*(nr+2)]*(nx+2)
a_e = [[0.0]*(nr+2)]*(nx+2)
a_w = [[0.0]*(nr+2)]*(nx+2)
a_n = [[0.0]*(nr+2)]*(nx+2)
a_s = [[0.0]*(nr+2)]*(nx+2)
b = [[0.0]*(nr+2)]*(nx+2)


# Matrix coefficients
for i in range(1, nx+1):
    for j in range(1, nr+1):
        a_p0[i][j] = r[j]*dr[j]*dx[i]/dt
        a_e[i][j] = 0
        a_w[i][j] = (1.0 - r[j]*r[j])*r[j]*dr[j]
        a_n[i][j] = (r[j] + 0.5*dr[j])*alfa_r*dx[i]/(0.5*dr[j] + 0.5*dr[j+1])
        a_s[i][j] = (r[j] - 0.5*dr[j])*alfa_r*dx[i]/(0.5*dr[j] + 0.5*dr[j-1])
        a_p[i][j] = a_p0[i][j] + a_w[i][j] + a_e[i][j] + a_n[i][j] + a_s[i][j]
        
t = 0.0
# Time stepping
while t<t_end:
    t += dt
    tol = 1.0
    rel_tol = 1.0e-5
    
    while tol > rel_tol:
        tol = 0.0
        for i in range(1, nx+1):
            for j in range(1, nr+1):
                res = T[i][j] - (a_e[i][j]*T[i+1][j] + a_w[i][j]*T[i-1][j] + a_n[i][j]*T[i][j+1] + a_s[i][j]*T[i][j-1] + a_p0[i][j]*T[i][j])/a_p[i][j]
                T[i][j] = T[i][j] - res
                tol += abs(res)
                
        print tol
                
print T
                
        


#plot(x)
#plot(r)
#show()




