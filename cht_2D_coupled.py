import time
import math
import numpy as np
from pylab import *

def write_arrays(f_name, open_type, arr1):
        f = open(f_name, open_type)
        s = ''
        
        for j in range(0, len(arr1)):
                s += str(arr1[j]) + ','
                
        s += '\n'
        f.write(s)
        f.close()
                                                                                                                

def TDMA(aw,ae,ap,q,res,nx,type):
	P = [0]*nx
	Q = [0]*nx

	if type == "val":
		P[0] = 0.
		Q[0] = res[0]
		
	for i in range(1, nx-1):
		denom = 1./(aw[i]*P[i-1] + ap[i])
	
		P[i] = -ae[i] * denom
		Q[i] = (q[i] - aw[i]*Q[i-1]) * denom
	
	for i in range(nx-2, 0, -1):
		res[i] = P[i]*res[i+1] + Q[i]
		

def integrate(t_0, t_end):
	t = t_0

	# Time stepping
	while t < t_end:
		t += dt
		print t
		
		tol = 1.0
		iteration = 0
		
		# Iterative inner-time solver
		while tol > rel_tol:
			iteration += 1
			tol = 0.0

			for i in range(1, nx+1):
				for j in range(nr, 0, -1):
					res = T[i][j] - (a_n[i][j]*T[i][j+1] + a_s[i][j]*T[i][j-1] + a_p0[i][j]*T0[i][j] + a_e[i][j]*T[i+1][j] + a_w[i][j]*T[i-1][j])/a_p[i][j]
					T[i][j] -= res

			for j in range(nr, 0, -1):
				for i in range(1, nx+1):
					res = T[i][j] - (a_n[i][j]*T[i][j+1] + a_s[i][j]*T[i][j-1] + a_p0[i][j]*T0[i][j] + a_e[i][j]*T[i+1][j] + a_w[i][j]*T[i-1][j])/a_p[i][j]
					T[i][j] -= res
					tol += abs(res)
					
		for i in range(1, nx+1):
			for j in range(1, nr+1):
				T0[i][j] = T[i][j]

		# Interpolate T / axis and right BC
		for i in range(1, nx+1):
			T[i][0] = T[i][1]

		for j in range(1, nr+1):
			T[nx+1][j] = T[nx][j]
	


start_time = time.time()

# Hydrodynamic and thermophysical properties
alfa_r = 0.0051
alfa_f = 1.0
alfa_s = alfa_r*alfa_f
k_r = 7.48
k_f = 1.0
k_s = k_f*k_r
rhoC_f = 1.0
rhoC_s = k_r/alfa_r*rhoC_f
conv_f = 10.0
T_left = 1.0
T_top = 0.0
T_init = 0.0


# Numerical settings
rel_tol = 1.0e-4

# Dimensioneless calculation
t_0 = 0.0
t_end = 0.018
dt = 0.05

nr = 40
nx = 50
eps = 1.2

r_0 = 0.0
r_int = 1.0
r_max = 1.2
x_max = 1.57

# Total amount of the points with the boundaries
dx = [0.0]*(nx + 2)
x = [0.0]*(nx + 2)
dr = [0.0]*(nr + 2)
r = [0.0]*(nr + 2)


# Geometrical progression of the dx distribution
eps_sum = 0.0
for i in range(0, nx):
	eps_sum += pow(eps, i)

	dx[1] = x_max/eps_sum
	x[1] = 0.5*dx[1]
	
	for i in range(2, nx+1):
		dx[i] = dx[i-1]*eps
		x[i] = x[i-1] + 0.5*dx[i-1] + 0.5*dx[i]
		
		x[nx+1] = x[nx] + dx[nx]*0.5

print x
                
# Geometrical progression of the dr distribution
rel = (r_max - r_int)/r_int
nr_int = 0

eps_sum_f = 0.0
for i in range(0, nr):
        eps_sum_f += pow(eps, i)

eps_sum_s = 0.0
for i in range(0, nr):
    eps_sum_f -= pow(eps, nr-1-i)
    eps_sum_s += pow(eps, i)
    if(rel - eps_sum_s/eps_sum_f < 0):
        nr_int = i
        break

nr_int = nr-nr_int
    
eps_sum_s = 0.0
for i in range(nr_int, nr):
    eps_sum_s += pow(eps, i-nr_int)

eps_sum_f = 0.0
for i in range(0, nr_int):
    eps_sum_f += pow(eps, i)

dr[nr_int] = r_int/eps_sum_f
dr[nr_int+1] = (r_max - r_int)/eps_sum_s

r[nr_int] = r_int - 0.5*dr[nr_int]
r[nr_int+1] = r[nr_int] + 0.5*dr[nr_int] + 0.5*dr[nr_int+1]

for i in range(1, nr_int):
    dr[nr_int-i] = dr[nr_int+1-i]*eps
    r[nr_int-i] = r[nr_int+1-i] - 0.5*dr[nr_int-i] - 0.5*dr[nr_int-i+1]

r[0] = r[1] - 0.5*dr[1]


for i in range(nr_int+2, nr+1):
    dr[i] = dr[i-1]*eps
    r[i] = r[i-1] + 0.5*dr[i-1] + 0.5*dr[i]
    
r[nr+1] = r[nr]+0.5*dr[nr]

# Arrays of hydrodynamic and thermophysical properties
conv = [0.0]*(nr+2)
alfa = [0.0]*(nr+2)
k = [0.0]*(nr+2)
rhoC = [0.0]*(nr+2)

for j in range(0, nr+2):
	if(j <= nr_int):
		alfa[j] = alfa_f
		conv[j] = conv_f
		rhoC[j] = rhoC_f
		k[j] = k_f
	else:
		conv[j] = 0.0
		alfa[j] = alfa_s
		rhoC[j] = rhoC_s
		k[j] = k_s



T = [[0.0]*(nr+2) for i in range(nx+2)]
T0 = [[0.0]*(nr+2) for i in range(nx+2)]

# Boundary conditions
for j in range(0, nr+2):
	T[0][j] = T_left

for i in range(0, nx+2):
	T[i][nr+1] = T_top

# Initial conditions
for i in range(1, nx+1):
	for j in range(1, nr+1):
		T[i][j] = T_init
		T0[i][j] = T_init


a_p = [[0.0]*(nr+2) for i in range(nx+2)]
a_e = [[0.0]*(nr+2) for i in range(nx+2)]
a_w = [[0.0]*(nr+2) for i in range(nx+2)]
a_n = [[0.0]*(nr+2) for i in range(nx+2)]
a_s = [[0.0]*(nr+2) for i in range(nx+2)]
a_p0 = [[0.0]*(nr+2) for i in range(nx+2)]
b = [[0.0]*(nr+2) for i in range(nx+2)]

# Matrix coefficients
for i in range(1, nx+1):
	for j in range(1, nr+1):
		k_n = k[j]
		k_s = k[j-1]
		if(j == nr_int):
			f_n = dr[j+1]/(dr[j] + dr[j+1])
			k_n = 1.0/((1.0-f_n)/k[j] + f_n/k[j+1])
#			print k_n

		if(j == nr_int+1):
			f_s = dr[j-1]/(dr[j-1] + dr[j])
			k_s = 1.0/((1.0-f_s)/k[j] + f_s/k[j-1])
#			print k_s
		   
		a_p0[i][j] = rhoC[j]*r[j]*dr[j]*dx[i]/dt
		a_n[i][j] = k_n*(r[j] + 0.5*dr[j])*dx[i]/(0.5*dr[j] + 0.5*dr[j+1])
		a_s[i][j] = k_s*(r[j] - 0.5*dr[j])*dx[i]/(0.5*dr[j] + 0.5*dr[j-1])
		a_e[i][j] = 0.0
		a_w[i][j] = rhoC[j]*conv[j]*(1.0 - r[j]*r[j])*r[j]*dr[j]
		a_p[i][j] = a_p0[i][j] + a_n[i][j] + a_s[i][j] + rhoC[j]*conv[j]*(1.0 - r[j]*r[j])*r[j]*dr[j]

path = '2d_coupled.csv'
write_arrays(path, 'w', r)

integrate(t_0=0.0, t_end=0.1)
write_arrays(path, 'a', T[2])

integrate(t_0=0.1, t_end=0.5)
write_arrays(path, 'a', T[2])

integrate(t_0=0.5, t_end=1.0)
write_arrays(path, 'a', T[2])

integrate(t_0=1.0, t_end=2.0)
write_arrays(path, 'a', T[2])

print("--- %s seconds ---" % (time.time() - start_time))
