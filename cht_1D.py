import math
import numpy as np
from pylab import *

def integrate(t_0,t_end):
	t = t_0
	# Time stepping
	while t<t_end:
		t += dt
		
		tol = 1.0
		k = 0
		while tol > rel_tol:
			k += 1
			tol = 0.0

			for j in range(1, nr+1):
				res = T[j] - (a_n[j]*T[j+1] + a_s[j]*T[j-1] + a_p0[j]*T0[j])/a_p[j]
				T[j] -= res
				tol += abs(res)

		for j in range(1, nr+1):
			T0[j] = T[j]

		# Interpolate T
		T[0] = T[1]


            
# Thermophysical properties
alfa_r = 1.0

# Numerical settings
rel_tol = 1.0e-6

# Dimensioneless calculation
t_0 = 0.0
t_end = 1.0e-2
dt = 1.0e-4

nr = 40
eps = 1.2

r_0 = 0.0
r_int = 1.0
r_max = 1.2

# Total amount of the points with the boundaries
dx = 0.1
dr = [0.0]*(nr + 2)
r = [0.0]*(nr + 2)

# Geometrical progression of the dr distribution
rel = (r_max - r_int)/r_int
nr_int = 0

eps_sum_f = 0.0
for i in range(0, nr):
	eps_sum_f += pow(eps, i)

eps_sum_s = 0.0
for i in range(0, nr):
	eps_sum_f -= pow(eps, nr-1-i)
	eps_sum_s += pow(eps, i)
	if(rel - eps_sum_s/eps_sum_f < 0):
		nr_int = i
		break

nr_int = nr-nr_int

eps_sum_s = 0.0
for i in range(nr_int, nr):
	eps_sum_s += pow(eps, i-nr_int)

eps_sum_f = 0.0
for i in range(0, nr_int):
	eps_sum_f += pow(eps, i)
	
dr[nr_int] = r_int/eps_sum_f
dr[nr_int+1] = (r_max - r_int)/eps_sum_s

r[nr_int] = r_int - 0.5*dr[nr_int]
r[nr_int+1] = r[nr_int] + 0.5*dr[nr_int] + 0.5*dr[nr_int+1]

for i in range(1, nr_int):
	dr[nr_int-i] = dr[nr_int+1-i]*eps
	r[nr_int-i] = r[nr_int+1-i] - 0.5*dr[nr_int-i] - 0.5*dr[nr_int-i+1]

r[0] = r[1] - 0.5*dr[1]

for i in range(nr_int+2, nr+1):
	dr[i] = dr[i-1]*eps
	r[i] = r[i-1] + 0.5*dr[i-1] + 0.5*dr[i]
	
r[nr+1] = r[nr]+0.5*dr[nr]


T = [0.0]*(nr+2)
T0 = [0.0]*(nr+2)

# Boundary conditions
T[nr+1] = 0.0

# Initial conditions
for j in range(1, nr+1):
    T[j] = T0[j] = 1.0

a_p = [0.0]*(nr+2)
a_e = [0.0]*(nr+2)
a_w = [0.0]*(nr+2)
a_n = [0.0]*(nr+2)
a_s = [0.0]*(nr+2)
a_p0 = [0.0]*(nr+2)
b = [0.0]*(nr+2)


# Matrix coefficients
for j in range(1, nr+1):
    a_p0[j] = r[j]*dr[j]*dx/dt
    a_n[j] = alfa_r*(r[j] + 0.5*dr[j])*dx/(0.5*dr[j] + 0.5*dr[j+1])
    a_s[j] = alfa_r*(r[j] - 0.5*dr[j])*dx/(0.5*dr[j] + 0.5*dr[j-1])
    a_p[j] = a_p0[j] + a_n[j] + a_s[j]

    
integrate(t_0=0.0, t_end=1.0e-4)
plot(r,T,'bo',r,T)

integrate(t_0=1.0e-4, t_end=1.0e-2)
plot(r,T,'ro',r,T)

show()




